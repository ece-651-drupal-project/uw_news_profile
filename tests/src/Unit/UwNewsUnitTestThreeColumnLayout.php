<?php

namespace Drupal\Tests\uw_news_profile\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Form\FormState;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\uw_dashboard\Plugin\Layout\Uw3ColumnLayout;
use Drupal\Tests\UnitTestCase;

/**
 * Tests for create users form.
 *
 * @group create_users
 */
class UwNewsUnitTestThreeColumnLayout extends UnitTestCase {

  /**
   * The string translation object.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $stringTranslation;

  /**
   * Setup tests.
   */
  public function setUp(): void {

    $this->stringTranslation = $this->createMock('Drupal\Core\StringTranslation\TranslationInterface');
  }

  /**
   * Test UW News block options.
   */
  public function testThreeColumnLayout(): void {

    print PHP_EOL . 'Three column layout (test options)' . PHP_EOL;

    // The actual values to test.
    $actual = [];

    // The expected values to test.
    $expected = [];

    // Set a Drupal container.
    $container = new ContainerBuilder();
    $container->set('string_translation', $this->stringTranslation);
    \Drupal::setContainer($container);

    // The configuration for layout.
    $configuration = [];

    // The plugin definition for layout.
    $plugin_definition = [
      'deriver' => NULL,
      'label' => new TranslatableMarkup('Three Columns'),
      'description' => NULL,
      'category' => new TranslatableMarkup('UW layouts'),
      'template' => 'layout--uw-3-col',
      'templatePath' => 'profiles/uw_news_profile/modules/features/uw_dashboard/layouts/uw-3-col',
      'theme_hook' => 'layout__uw_3_col',
      'path' => 'profiles/uw_news_profile/modules/features/uw_dashboard',
      'library' => 'uw_dashboard/uw_layout_3_col',
      'icon' => NULL,
      'icon_map' => [
        ['first', 'second', 'third'],
      ],
      'regions' => [
        'first' => [
          'label' => new TranslatableMarkup('First', [], ['context' => 'layout_region']),
        ],
        'second' => [
          'label' => new TranslatableMarkup('Second', [], ['context' => 'layout_region']),
        ],
        'third' => [
          'label' => new TranslatableMarkup('Third', [], ['context' => 'layout_region']),
        ],
      ],
      'default_region' => 'first',
      'additional' => [],
      'id' => 'uw_3_column',
      'class' => 'Drupal\uw_dashboard\Plugin\Layout\Uw3ColumnLayout',
      'provider' => 'uw_dashboard',
      'config_dependencies' => [
        'module' => ['uw_dashboard'],
      ],
    ];

    // The plugin id for layout.
    $plugin_id = 'uw_3_column';

    // Get the layout.
    $layout = new Uw3ColumnLayout($configuration, $plugin_id, $plugin_definition);

    // Get the configuration form for the layout.
    $conf_form = $layout->buildConfigurationForm([], new FormState());

    // Get the options for the layout.
    $options = $conf_form['layout_settings']['column_class']['#options'];

    // Step through each filter and get the option, have to get
    // out of Translatable Markup.
    foreach ($options as $key => $option) {
      $actual[$key] = $option->getUntranslatedString();
    }

    // The expected values for the layout.
    $expected = [
      'even-split' => 'Even split (33%, 34%, 33%)',
      'larger-left' => 'Larger left (50%, 25%, 25%)',
      'larger-middle' => 'Larger middle (25%, 50%, 25%)',
      'larger-right' => 'Larger right (25%, 25%, 50%)',
    ];

    // Test that the options for max items are the same.
    $this->assertSame($expected, $actual);
    print '     Assertion: options for three column layout' . PHP_EOL;
  }

}
