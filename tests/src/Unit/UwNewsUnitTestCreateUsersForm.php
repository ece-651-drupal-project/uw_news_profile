<?php

namespace Drupal\Tests\phpunit_example\Unit;

use Drupal\Core\Form\FormState;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\uw_news_common\Form\CreateUsersForm;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Tests for create users form.
 *
 * @group create_users
 */
class UwNewsUnitTestCreateUsersForm extends UnitTestCase {

  /**
   * Translation object.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  private $translationInterfaceMock;

  /**
   * UW news create users form object.
   *
   * @var \Drupal\uw_news_common\Form
   */
  private $form;

  /**
   * Drupal messenger object.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $messenger;

  /**
   * Language manager object.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $languageManager;

  /**
   * CSRF token object.
   *
   * @var \Drupal\Core\Access\CsrfTokenGenerator|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $csrfToken;

  /**
   * Drupal logger object.
   *
   * @var \PHPUnit\Framework\MockObject\MockObject|\Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Form error handler object.
   *
   * @var \Drupal\Core\Form\FormErrorHandlerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $formErrorHandler;

  /**
   * Setup tests.
   */
  public function setUp(): void {

    // Mock the messenger, language, csrf token, logger and
    // for error handler services.
    $this->messenger = $this->createMock('Drupal\Core\Messenger\MessengerInterface');
    $this->languageManager = $this->createMock('Drupal\Core\Language\LanguageManagerInterface');
    $this->csrfToken = $this->createMock('\Drupal\Core\Access\CsrfTokenGenerator');
    $this->logger = $this->createMock('\Psr\Log\LoggerInterface');
    $this->formErrorHandler = $this->createMock('Drupal\Core\Form\FormErrorHandlerInterface');
    $this->translationInterfaceMock = $this->prophesize(TranslationInterface::class);

    // Instantiate the form to be tested.
    $this->form = new CreateUsersForm($this->messenger, $this->languageManager);

    // Config Base Form has a call to $this->t() which references
    // the TranslationService.
    // Set the translation service mock so that the program won't
    // throw an error.
    $this->form->setStringTranslation($this->translationInterfaceMock->reveal());
  }

  /**
   * Test that the correct form ID is returned.
   */
  public function testFormId(): void {

    print PHP_EOL . 'Create users (test form id)' . PHP_EOL;

    $this->assertEquals('create_users_form', $this->form->getFormId());
    print '     Assertion: form id is correct' . PHP_EOL;
  }

  /**
   * Test that the form passes validation.
   */
  public function testCreateUsersFormValidation(): void {

    print PHP_EOL . 'Create users (test form validation)' . PHP_EOL;

    // Load the form validator.
    $form_validator = $this->getMockBuilder('Drupal\Core\Form\FormValidator')
      ->setConstructorArgs(
        [
          new RequestStack(),
          $this->getStringTranslationStub(),
          $this->csrfToken,
          $this->logger,
          $this->formErrorHandler,
        ]
      )
      ->setMethods(NULL)
      ->getMock();

    // Create variables for form and form state.
    $form = [];
    $form_state = new FormState();

    // Build the form.
    $form = $this->form->buildForm($form, $form_state);

    // Set a wrong value (<1 or > 1000) for the form and
    // ensure that it fails validation.
    $form_state = (new FormState())->setValue('num_user', 100000);
    $form_validator->validateForm($this->form->getFormId(), $form, $form_state);
    $this->assertEmpty($form_state->hasAnyErrors());
    print '     Assertion: validation incorrect number' . PHP_EOL;

    // Set another wrong value using characters for the and
    // ensure that it fails validation.
    $form_state = (new FormState())->setValue('num_user', 'zzzz');
    $form_validator->validateForm($this->form->getFormId(), $form, $form_state);
    $this->assertEmpty($form_state->hasAnyErrors());
    print '     Assertion: validation using characters' . PHP_EOL;

    // Set a correct value and ensure form passes validation.
    $form_state = (new FormState())->setValue('num_user', 100);
    $form_validator->validateForm($this->form->getFormId(), $form, $form_state);
    $this->assertFalse($form_state->hasAnyErrors());
    print '     Assertion: passes validation' . PHP_EOL;
  }

}
