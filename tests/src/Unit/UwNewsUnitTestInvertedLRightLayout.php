<?php

namespace Drupal\Tests\uw_news_profile\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Form\FormState;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Tests\UnitTestCase;
use Drupal\uw_dashboard\Plugin\Layout\UwInvertedLLeftLayout;

/**
 * Tests for create users form.
 *
 * @group create_users
 */
class UwNewsUnitTestInvertedLRightLayoutLayout extends UnitTestCase {

  /**
   * The string translation object.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $stringTranslation;

  /**
   * Setup tests.
   */
  public function setUp(): void {

    $this->stringTranslation = $this->createMock('Drupal\Core\StringTranslation\TranslationInterface');
  }

  /**
   * Test UW News block options.
   */
  public function testInvertedRightLayout(): void {

    print PHP_EOL . 'Inverted L Right layout (test options)' . PHP_EOL;

    // The actual values to test.
    $actual = [];

    // The expected values to test.
    $expected = [];

    // Set a Drupal container.
    $container = new ContainerBuilder();
    $container->set('string_translation', $this->stringTranslation);
    \Drupal::setContainer($container);

    // The configuration for layout.
    $configuration = [];

    // The plugin definition for layout.
    $plugin_definition = [
      'deriver' => NULL,
      'label' => new TranslatableMarkup('Inverted "L" - right'),
      'description' => NULL,
      'category' => new TranslatableMarkup('UW layouts'),
      'template' => 'layout--uw-inverted-l-right',
      'templatePath' => 'profiles/uw_news_profile/modules/features/uw_dashboard/layouts/uw-inverted-l-right',
      'theme_hook' => 'layout__uw_inverted_l_right',
      'path' => 'profiles/uw_news_profile/modules/features/uw_dashboard',
      'library' => 'uw_dashboard/uw_layout_inverted_l_right',
      'icon' => NULL,
      'icon_map' => [
        ['first', 'fourth', 'fourth'],
        ['first', 'second', 'third'],
      ],
      'regions' => [
        'first' => [
          'label' => new TranslatableMarkup('First', [], ['context' => 'layout_region']),
        ],
        'second' => [
          'label' => new TranslatableMarkup('Second', [], ['context' => 'layout_region']),
        ],
        'third' => [
          'label' => new TranslatableMarkup('Third', [], ['context' => 'layout_region']),
        ],
        'fourth' => [
          'label' => new TranslatableMarkup('Fourth', [], ['context' => 'layout_region']),
        ],
      ],
      'default_region' => 'first',
      'additional' => [],
      'id' => 'uw_inverted_l_right',
      'class' => 'Drupal\uw_dashboard\Plugin\Layout\UwInvertedLRightLayout',
      'provider' => 'uw_dashboard',
      'config_dependencies' => [
        'module' => ['uw_dashboard'],
      ],
    ];

    // The plugin id for layout.
    $plugin_id = 'uw_inverted_l_right';

    // Get the layout.
    $layout = new UwInvertedLLeftLayout($configuration, $plugin_id, $plugin_definition);

    // Get the configuration form for the layout.
    $conf_form = $layout->buildConfigurationForm([], new FormState());

    // Get the options for the layout.
    $options = $conf_form['layout_settings']['column_class']['#options'];

    // Step through each filter and get the option, have to get
    // out of Translatable Markup.
    foreach ($options as $key => $option) {
      $actual[$key] = $option->getUntranslatedString();
    }

    // The expected values for the layout.
    $expected = [
      'even-split' => 'Even split (50%, 50%)',
      'larger-left' => 'Larger left (33%, 67%)',
      'larger-right' => 'Larger right (67%, 33%)',
    ];

    // Test that the options for layout are the same.
    $this->assertSame($expected, $actual);
    print '     Assertion: options for inverted L right layout' . PHP_EOL;
  }

}
