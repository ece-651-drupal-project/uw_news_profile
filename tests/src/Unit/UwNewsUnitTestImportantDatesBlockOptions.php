<?php

namespace Drupal\Tests\phpunit_example\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Form\FormState;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Tests\UnitTestCase;
use Drupal\uw_dashboard\Plugin\Block\UwImportantDatesBlock;

/**
 * Tests for UW Important Dates block.
 *
 * @group events_block
 */
class UwNewsUnitTestImportantDatesBlockOptions extends UnitTestCase {

  /**
   * The http client object.
   *
   * @var \GuzzleHttp\ClientInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $httpClient;

  /**
   * The string translation object.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $stringTranslation;

  /**
   * Setup tests.
   */
  public function setUp(): void {

    $this->stringTranslation = $this->createMock('Drupal\Core\StringTranslation\TranslationInterface');
    $this->httpClient = $this->createMock('GuzzleHttp\ClientInterface');
  }

  /**
   * Test UW Important Dates block options.
   */
  public function testUwImportantDatesBlockOptions(): void {

    print PHP_EOL . 'UW Important Dates Block (test options)' . PHP_EOL;

    // Set a Drupal container.
    $container = new ContainerBuilder();
    $container->set('string_translation', $this->stringTranslation);
    \Drupal::setContainer($container);

    // Set the config for this UW Important Dates block.
    $configuration = [
      'id' => 'uw_news_block_events',
    ];

    // Set the plugin definition for UW Important Dates block.
    $plugin_definition = [
      'admin_label' => new TranslatableMarkup('UW Important Dates'),
      'category' => 'UW News Dashboard items',
      'context_definitions' => [],
      'id' => 'uw_news_block_events',
      'class' => 'Drupal\uw_dashboard\Plugin\Block\UwImportantDatesBlock',
      'provider' => 'uw_dashboard',
    ];

    // Set the plugin id for UW Important Dates block.
    $plugin_id = 'uw_news_block_important_dates';

    // Load the UW Important Dates block.
    $block = new UwImportantDatesBlock($configuration, $plugin_id, $plugin_definition, $this->httpClient);

    // Get the block form for UW Important Dates.
    $block_form = $block->blockForm([], new FormState());

    // Get the options for max items.
    $actual = $block_form['max_items']['#options'];

    // Reset expected array.
    $expected = [];

    // Set the expected values.
    for ($i = 1; $i <= 25; $i++) {
      $expected[$i] = $i;
    }

    // Test that the options for max items are the same.
    $this->assertSame($expected, $actual);
    print '     Assertion: max items options' . PHP_EOL;

    // Get the default value for max chars.
    $actual = $block_form['max_chars']['#default_value'];

    // Set the expected value for max chars.
    $expected = 400;

    // Test that the options for max items are the same.
    $this->assertSame($expected, $actual);
    print '     Assertion: max chars default value' . PHP_EOL;
  }

}
