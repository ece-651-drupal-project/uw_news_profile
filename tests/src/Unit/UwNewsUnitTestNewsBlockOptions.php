<?php

namespace Drupal\Tests\phpunit_example\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Form\FormState;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\uw_dashboard\Plugin\Block\UwNewsBlock;
use Drupal\Tests\UnitTestCase;

/**
 * Tests for create users form.
 *
 * @group create_users
 */
class UwNewsUnitTestNewsBlockOptions extends UnitTestCase {

  /**
   * The http client object.
   *
   * @var \GuzzleHttp\ClientInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $httpClient;

  /**
   * The string translation object.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $stringTranslation;

  /**
   * Setup tests.
   */
  public function setUp(): void {

    $this->stringTranslation = $this->createMock('Drupal\Core\StringTranslation\TranslationInterface');
    $this->httpClient = $this->createMock('GuzzleHttp\ClientInterface');
  }

  /**
   * Test UW News block options.
   */
  public function testUwNewsBlockOptions(): void {

    print PHP_EOL . 'UW News Block (test options)' . PHP_EOL;

    // Set a Drupal container.
    $container = new ContainerBuilder();
    $container->set('string_translation', $this->stringTranslation);
    \Drupal::setContainer($container);

    // Set the config for this UW News block.
    $configuration = [
      'id' => 'uw_news_block_uwnews',
    ];

    // Set the plugin definition for UW News block.
    $plugin_definition = [
      'admin_label' => new TranslatableMarkup('UW News'),
      'category' => 'UW News Dashboard items',
      'context_definitions' => [],
      'id' => 'uw_news_block_uwnews',
      'class' => 'Drupal\uw_dashboard\Plugin\Block\UwNewsBlock',
      'provider' => 'uw_dashboard',
    ];

    // Set the plugin id for UW News block.
    $plugin_id = 'uw_news_block_uwnews';

    // Load the UW News block.
    $block = new UwNewsBlock($configuration, $plugin_id, $plugin_definition, $this->httpClient);

    // Get the block form for UW News block.
    $block_form = $block->blockForm([], new FormState());

    // Get the filters options form the block form.
    $filters = $block_form['filter']['#options'];

    // Values to best tested against.
    $actual = [];

    // Step through each filter and get the option, have to get
    // out of Translatable Markup.
    foreach ($filters as $key => $filter) {
      $actual[$key] = $filter->getUntranslatedString();
    }

    // Set the expected values.
    $expected = [
      'all' => 'All',
      '39' => 'Arts',
      '142' => 'Engineering',
      '245' => 'Environment',
      '1240' => 'Graduate Studies and Postdoctoral Affairs',
      '11' => 'Health',
      '286' => 'Mathematics',
      '473' => "Registrar's Office",
      '287' => 'Science',
    ];

    // Test that the options for filters are the same.
    $this->assertSame($expected, $actual);
    print '     Assertion: filters options' . PHP_EOL;

    // Get the options for max items.
    $actual = $block_form['max_items']['#options'];

    // Reset expected array.
    $expected = [];

    // Set the expected values.
    for ($i = 1; $i <= 25; $i++) {
      $expected[$i] = $i;
    }

    // Test that the options for max items are the same.
    $this->assertSame($expected, $actual);
    print '     Assertion: max items options' . PHP_EOL;

    // Get the default value for max chars.
    $actual = $block_form['max_chars']['#default_value'];

    // Set the expected value for max chars.
    $expected = 400;

    // Test that the options for max items are the same.
    $this->assertSame($expected, $actual);
    print '     Assertion: max chars default value' . PHP_EOL;
  }

}
