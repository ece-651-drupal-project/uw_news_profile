echo "****************************************************"
echo "Setting up profile/core for testing ..."
echo "****************************************************"
echo ""

cd ../../profiles/uw_news_profile
cp phpunit_*.xml ../../core

echo "****************************************************"
echo "Done setting up profile/core for testing."
echo "****************************************************"
echo ""

echo "****************************************************"
echo "Setting up permissions ..."
echo "****************************************************"
echo ""

cd /var/www/html/web/
sudo chmod -R 777 core

echo ""
echo "****************************************************"
echo "Done setting up permissions."
echo "****************************************************"
echo ""

echo "**************************************************************"
echo "All actions have been completed."
echo "You can now run unit tests with the command:"
echo "./run_tests_unit.sh"
echo "**************************************************************"
